#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Gerion Entrup <gerion.entrup@flump.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import config
import logging
import subprocess

from dataclasses import dataclass
from collections import defaultdict
from functools import lru_cache


logger = logging.getLogger("backend")


@dataclass(unsafe_hash=True)
class File:
    name: str
    size: int
    date: int


@dataclass(unsafe_hash=True)
class Resp:
    zip: File = None
    hash: File = None

    @lru_cache
    def name_parts(self):
        return self.zip.name[: -len(".zip")].split("-")

    def get_romtype(self):
        return self.name_parts()[3].lower()

    def get_version(self):
        return self.name_parts()[1]

    def get_device(self):
        return self.name_parts()[4]

    def get(self):
        return {
            "datetime": self.zip.date,
            "filename": self.zip.name,
            "id": get_hash(self.hash),
            "romtype": self.get_romtype(),
            "size": self.zip.size,
            "url": config.ZIP_URL.strip("/") + "/" + self.zip.name,
            "version": self.get_version(),
        }


def execute_command(cmd):
    cmdstr = " ".join("'" + x + "'" for x in cmd)
    logger.debug(f"Execute {cmdstr}")
    return subprocess.run(cmd, capture_output=True)


def get_hash(hashfile):
    cmd = make_lftp_command([("cat", hashfile.name)])
    lftp = execute_command(cmd)
    return lftp.stdout.decode("UTF-8").split(" ")[0]


def make_lftp_command(cmds):
    open_cmd = [("open", config.ZIP_URL)]
    sub_cmd = "; ".join([" ".join(x) for x in open_cmd + cmds])
    return [config.LFTP_COMMAND, "-c", sub_cmd]


def get_files():
    cmd = make_lftp_command([("cls", "-l", "--time-style=%s")])
    lftp = execute_command(cmd)
    files = [
        x.strip().split(" ") for x in lftp.stdout.decode("UTF8").strip("\n").split("\n")
    ]
    return [File(name=" ".join(x[2:]), size=int(x[0]), date=int(x[1])) for x in files]


def map_files(files):
    responses = defaultdict(Resp)
    for f in sorted(files, key=lambda x: x.name):
        if f.name.endswith(".zip"):
            responses[f.name].zip = f
        if f.name.endswith(".zip.sha256sum"):
            responses[f.name[: -len(".sha256sum")]].hash = f
    for response in responses.values():
        assert response.zip is not None and response.hash is not None
    return list(responses.values())


if __name__ == "__main__":
    print([x.get() for x in map_files(get_files())])
