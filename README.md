Lineage OTA Proxy Server
========================

Proxy a http file listing to a correct Lineage OTA server.

Several web servers allow a fast setup of file listings.
This project interprets them an wraps them into a correct Lineage OTA server.

Internally, [lftp](https://lftp.yar.ru/) is used for the web parsing, so every http(s) URL is supported that is readable by lftp.

## Usage

1. Create a `config.py`. See `config.py.in` for an example.
2. Test general working with `python3 backend.py`.
3. Test the webserver with `python3 frontend.py`.
4. You can use [ota-test](https://gitlab.com/geri0n/ota-test) for testing.

## Expected file structure

This proxy server expects a specific file layout. The command
`lftp -c "open https://your-host/path; cls -l"` should result in an output like:
```
636740507 Feb  1 08:45 lineage-17.1-20210201-nightly-sailfish-signed.zip
      113 Feb  1 08:47 lineage-17.1-20210201-nightly-sailfish-signed.zip.sha256sum
```

Additionally, URLs like `https://your-host/path/lineage-17.1-20210501-nightly-sailfish-signed.zip` must be valid.

## Deployment

The application is a very simple Flask server. See its [deployment documentation](https://flask.palletsprojects.com/en/1.1.x/deploying/) for further infos.
For a deployment with Gunicorn and systemd some sample files are laying in the systemd folder.
Please adjust the variables for your own needs.
