#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Gerion Entrup <gerion.entrup@flump.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import backend
import config
import logging

from flask import Flask, jsonify, Response

app = Flask(__name__)
logging_kwargs = {
    'level': config.LOG_LEVEL,
    'format': f"%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s",
}
if config.LOG_FILE is not None:
    logging_kwargs['filename'] = config.LOG_FILE
logging.basicConfig(**logging_kwargs)


@app.route("/v1/<device>/<rom_type>/<num>")
@app.route("/api/v1/<device>/<rom_type>/<num>")
def get(device, rom_type, num):
    files = backend.get_files()
    responses = backend.map_files(files)
    app.logger.debug(f"get(), got raw responses {responses}")
    responses = filter(lambda x: x.get_device() == device, responses)
    responses = filter(lambda x: x.get_romtype() == rom_type.lower(), responses)
    return jsonify({"response": [x.get() for x in responses]})


@app.route("/metrics")
@app.route("/api/metrics")
def metrics():
    app.logger.info("metrics() is not implemented")
    return Response({})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
